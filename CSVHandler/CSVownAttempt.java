
/**
 * @author dariush
 *
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.io.FileReader;
import java.io.IOException;

public class CSVownAttempt {

	private String file = "studentName.csv"; // muss sich im ProjektOrdner befinden!
	private String delimiter = ";";
	private String line = "";

	// Constructor 1
	public CSVownAttempt() {
	}

	// Constructor 2
	public CSVownAttempt(String delimiter, String file) {
		super();
		this.delimiter = delimiter;
		this.file = file;
	}

	// Begin Methods

	public List<Schueler> getAll() {
		Schueler s = null;
		List<Schueler> students = new ArrayList<Schueler>();

		// Add your code here
		try {
			BufferedReader reader = new BufferedReader(new FileReader("studentName.csv"));
			String zeile = "";

			// Throw away first line - Kopfzeile auslesen und wegwerfen
			reader.readLine();
			
			// Kopfzeile manuell printen
			System.out.printf("%-16s%s\t%-8s\t%s%n", "Name", "Joker", "Blamieren", "Frage");

			while ((zeile = reader.readLine()) != null) {
				// System.out.println(zeile);

				String[] splitted = zeile.split(";");
				
				// Durch Splitted gehen, jeden Wert kurzweilig in token speisen und printen
//				for (String token : splitted) {
//					System.out.println(token);
//				}
			
				System.out.printf("%-16s%s\t%s\t\t%s\n", splitted[1] + " " + splitted[0], splitted[2], splitted[3], splitted[4]);

//				System.out.printf("%-16s%s\t%-8s\t%s%n", splitted[0] + " " + splitted[1] + Integer.parseInt(splitted[2])
//						+ Integer.parseInt(splitted[3]) + Integer.parseInt(splitted[4]));

				String vorname = splitted[0];
				String nachname = splitted[1];
				String joker = splitted[2];
				String blamiert = splitted[3];
				String fragen = splitted[4];
				String name = splitted[0] + " " + splitted[1];
				
				s = new Schueler(name, Integer.parseInt(joker), Integer.parseInt(blamiert), Integer.parseInt(fragen));
				
				students.add(s);
				
			}
			reader.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Datei wurde nicht gefunden.");
		} catch (IOException e) {
			e.printStackTrace();
		}

		return students;
	}

	public void printAll(List<Schueler> students) {
		for (Schueler s : students) {
			System.out.println(s.getName());
		}
	}
}