
/**
 * @author dariush
 *
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.io.FileReader;
import java.io.IOException;

public class CSVHandler {
	
	private String[] titles;

	private String file = "studentName.csv"; // muss sich im ProjektOrdner befinden!
	private String delimiter = ";";
	private String line = "";

	// Constructor 1
	public CSVHandler() {
	}

	// Constructor 2
	public CSVHandler(String delimiter, String file) {
		super();
		this.delimiter = delimiter;
		this.file = file;
	}

	// Begin Methods

	public List<Schueler> getAll() {
		Schueler s = null;
		List<Schueler> students = new ArrayList<Schueler>();

		try {
			BufferedReader reader = new BufferedReader(new FileReader("studentName.csv"));
			String zeile = "";
			
			int count = -1;

			while ((zeile = reader.readLine()) != null) {
				String[] frags = zeile.split(";");
				
				if(isNumeric(frags[2])) {
					Schueler ss = new Schueler(frags[1] + " " + frags[0], Integer.parseInt(frags[2]), Integer.parseInt(frags[3]), Integer.parseInt(frags[4]));
					
					students.add(ss);
				} else {
					titles = frags;
				}
			}

			reader.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return students;
	}

	public void printAll(List<Schueler> students) {
		System.out.printf("%-15s %-15s %-15s %-15s %n", titles[1], titles[2], titles[3], titles[4]);
		
		for (Schueler s : students) {
			System.out.printf("%-15s %-15s %-15s %-15s %n", s.getName(), s.getJoker(), s.getBlamiert(), s.getFragen());

		}
	}
	
	private boolean isNumeric(String s) {
		try {
			Integer.parseInt(s);
			
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
}