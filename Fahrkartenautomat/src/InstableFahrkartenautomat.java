﻿import java.util.Scanner;

class InstableFahrkartenautomat
{
	
    static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args) {
    	
       boolean wiederholung = true;
       
       while(wiederholung == true) {
       double rueckgabebetrag;
       byte auswahlTicket = fahrkartenAuswahl();
       rueckgabebetrag = fahrkartenBezahlen(fahrkartenbestellungErfassen(auswahlTicket));
       fahrkartenAusgeben();
       rueckgeldAusgeben(rueckgabebetrag);

       System.out.printf("Möchten Sie noch ein Ticket kaufen?\n"
           + "(1) Ja\n"
           + "(2) Nein\nEingabe: ");
       
       byte auswahlWiederholung = tastatur.nextByte();
       
       while(auswahlWiederholung <= 0 || auswahlWiederholung >= 3) {
         System.out.println("Bitte probiere es erneut:");
         auswahlWiederholung = tastatur.nextByte();
       }
       
       if(auswahlWiederholung == 2) {
         wiederholung = false;
         System.out.println("Auf Wiedersehen!");
         } else {
           System.out.printf("\nAuf ein Neues!\n\n");
         }

       }
       
    }
    
    public static byte fahrkartenAuswahl() {
    	System.out.printf("Bitte wählen Sie mit der Eingabe der entsprechenden Zahl Ihr Ticket aus.\n "
    			+ "(1) Einzelfahrschein Regeltarif AB [2,90 EUR]\n "
    			+ "(2) Tageskarte Regeltarif AB [8,60 EUR]\n "
    			+ "(3) Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]\n"
    			+ "Hier eingeben: ");
    	byte auswahlTicket = tastatur.nextByte();
    	while(auswahlTicket >= 4 || auswahlTicket <= 0) {
    		System.out.println("Es wurde keine entsprechende Auswahl getroffen.\n"
    		    + "Versuchen Sie es bitte erneut: ");
        	auswahlTicket = tastatur.nextByte();
    	}
    	switch(auswahlTicket) {
    		case 1:
    			System.out.printf("\n(%d) Einzelfahrschein Regeltarif AB [2,90 EUR]\n", auswahlTicket);
    			break;
    		case 2:
    			System.out.printf("\n(%d) Tageskarte Regeltarif AB [8,60 EUR]\n", auswahlTicket);
    			break;
    		case 3:
    			System.out.printf("\n(%d) Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]\n", auswahlTicket);
    			break;
    		  default:
    			  System.out.println("Es wurde keine passende Auswahl getroffen.");
    			  break;
    	}
		return auswahlTicket;	  
    }
    
    public static double fahrkartenbestellungErfassen(byte auswahlTicket) {
    	
        short ticketAnzahl;
        double zuZahlenderBetrag = 0;
    	
    	if(auswahlTicket == 1) {
    		zuZahlenderBetrag = 2.9;
    	}
    	if(auswahlTicket == 2) {
    		zuZahlenderBetrag = 8.6;
    	}
    	if(auswahlTicket == 3) {
    		zuZahlenderBetrag = 23.5;
    	}
    	
        
        //Konsoleneingabe Zu zahlender Betrag 
        //System.out.print("Zu zahlender Betrag (EURO): ");
        //zuZahlenderBetrag = tastatur.nextDouble();
        
        //Konsoleneingabe Anzahl Tickets
        System.out.print("Anzahl der Tickets: ");
        ticketAnzahl = tastatur.nextShort();
        while(ticketAnzahl <= 0) {
        	System.out.print("\nBitte probieren Sie es erneut: ");
        	ticketAnzahl = tastatur.nextShort();
        }
        
        return ticketAnzahl*zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	
    	double restBetrag, eingeworfeneMünze;
    	
        double eingezahlterGesamtbetrag = 0;
        
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.print("Noch zu zahlen: ");
     	   restBetrag = zuZahlen - eingezahlterGesamtbetrag;
     	   
     	   System.out.printf("%.2f Euro%n", restBetrag);
     	   
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
     	   if(eingeworfeneMünze <= 2) {
     	   eingezahlterGesamtbetrag += eingeworfeneMünze;
        } else {
        	System.out.println("Bitte nur mit maximal 2€ Münzen bezahlen.");
        }
        }

    	return eingezahlterGesamtbetrag - zuZahlen;
    	
    }
    
    public static void fahrkartenAusgeben() {

        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
           System.out.print("=");
           try {
 			  Thread.sleep(250);
           } catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
           }
        }
        System.out.println("\n");
    	
    }
    
    public static void rueckgeldAusgeben(double restBetrag) {
    	
     	   System.out.printf("Der Rückgabebetrag in Höhe von ");
     	   System.out.printf("%.2f", restBetrag);
     	   System.out.println(" Euro wird in folgenden Münzen ausgezahlt:");

//     	   better solution: iterate over different coin values

//        String[] coinsValue = {
//            "2 Euro", "1 Euro", "50 Cent", "20 Cent", "10 Cent", "5 Cent", "2 Cent", "1 Cent" };
        
//        for (double coin : coins) {
//        for (int i = 0; i < coins.length; ++i) {
//          while (restBetrag >= coins[i] || coins[i] == 0.01 && restBetrag > 0) {
//            
//            restBetrag -= coins[i];
//            System.out.printf("%7s%n", coinsValue[i]);
//          }
//        }
      double[] coins = { 2.0, 1.0, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01};
      for (double coin : coins) {
        while (restBetrag >= coin || coin == 0.01 && restBetrag > 0) {
         // System.out.printf("%2d %s%n", (int) (coin >= 1.0 ? coin : coin * 100), coin >= 1.0 ? "Euro" : "Cent");
          restBetrag -= coin;
          if (coin >= 1.0) {
            System.out.printf("%2d Euro%n", (int) coin);
          } else {
            System.out.printf("%2d Cent%n", (int) (coin * 100));
          }
        }
      }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n");
        
        for (int i = 0; i < 4; i++) {
           System.out.print("=");
           try {
 			  Thread.sleep(500);
           } catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
           }
        }
        System.out.println("\n");
    }
}