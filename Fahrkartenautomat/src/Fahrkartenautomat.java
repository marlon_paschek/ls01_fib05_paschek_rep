﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       short ticketAnzahl;
       double newZuZahlenderBetrag;

       
       //Konsoleneingabe Zu zahlender Betrag 
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       //Konsoleneingabe Anzahl Tickets
       System.out.print("Anzahl der Tickets: ");
       ticketAnzahl = tastatur.nextShort();
       
       // Ticketanzahl mit Preis multiplizieren
       zuZahlenderBetrag = ticketAnzahl*zuZahlenderBetrag;

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.001;
       while(eingezahlterGesamtbetrag <= zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: ");
    	   newZuZahlenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
    	   System.out.printf("%.2f", newZuZahlenderBetrag);
    	   System.out.printf(" Euro\n");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
       
       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag >= 0.001)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von ");
    	   System.out.printf("%.2f", rückgabebetrag);
    	   System.out.println(" Euro wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.050)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
           while(rückgabebetrag >= 0.02)// 2 CENT-Münzen
           {
        	  System.out.println("2 CENT");
 	          rückgabebetrag -= 0.02;
           }
           while(rückgabebetrag >= 0.01)// 1 CENT-Münzen
           {
        	  System.out.println("1 CENT");
 	          rückgabebetrag -= 0.01;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
       // Demo: 1x2,10€ Ticket mit 2x2€ Münzen bezahlen
       System.out.println("\nFun-Fact:\n" +
       "Restgeldsumme beim Automaten:" + rückgabebetrag);
       
    }  
}