import java.util.Scanner;

public class Mittelwert {
    static Scanner tastatur = new Scanner(System.in);

   public static void main(String[] args) {
	   
	   System.out.println("Bitte geben Sie den 1. Wert ein: ");
      double x = tastatur.nextDouble();
	   System.out.println("Bitte geben Sie den 2. Wert ein: ");
      double y = tastatur.nextDouble();

      double m = berechneMittelwert(x,y);
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f.\n", x, y, m);
   }

   public static double berechneMittelwert(double x1, double x2) {
	  	return (x1 + x2) / 2;
	}
}