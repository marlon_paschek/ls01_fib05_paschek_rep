
public class FormatierteAusgabe3 {

	public static void main(String[] args) {	
		
		//%n instead of \n to create platform specific line end character
		//first argument has no spacing, fahrenheit is already 10 chars long
		System.out.printf("%s | %8s%n", "Fahrenheit", "Celsius");
		System.out.printf("----------------------%n");
		System.out.printf("%-10s | %8s%n", "-20", "-28.89");
		System.out.printf("%-10s | %8s%n", "-10", "-23.33");
		System.out.printf("%-10s | %8s%n", "+0", "-17.78");
		System.out.printf("%-10s | %8s%n", "+20", "-6.67");
		System.out.printf("%-10s | %8s%n", "+30", "-1.11");
		
	}
}
