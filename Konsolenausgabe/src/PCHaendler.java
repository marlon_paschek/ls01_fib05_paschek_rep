import java.util.Scanner;

public class PCHaendler {
	
    static Scanner myScanner = new Scanner(System.in);

  public static void main(String[] args) {
    String artikel = liesString();
    int anzahl = liesInt(artikel);
	  
    double nettopreis = liesDouble("Geben Sie den Nettopreis ein:");
    double mwst = liesDouble("Geben Sie die Mwst ein:");
    
    double gesamtNettopreis = berechneGesamtnettopreis(anzahl, nettopreis);
    double gesamtBruttopreis = berechneGesamtbruttopreis(gesamtNettopreis, mwst);
    
    rechnungAusgeben(artikel, anzahl, gesamtNettopreis, gesamtBruttopreis, mwst);
  }

  public static String liesString() {
	  System.out.println("Was m�chten Sie bestellen?");
	  String artikel = myScanner.next();
	  return artikel;
	}
  
  public static int liesInt(String text) {
	  System.out.println("Sie m�chten bestellen: " + text);
	  System.out.println("Geben Sie die Anzahl ein:");
	  return myScanner.nextInt();
	}
  
  public static double liesDouble(String text) { 
	  System.out.println(text);
	  return myScanner.nextDouble();
  }
  
  public static double berechneGesamtnettopreis(int anzahl, double preis) {
	    return anzahl * preis;
  }

  public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
    return nettogesamtpreis * (1 + mwst / 100);
  }
  
  public static void rechnungAusgeben(
      String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
    System.out.println("\t\t Rechnung");
    System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
    System.out.printf(
        "\t\t Brutto: %-20s %6d %10.2f (%.1f %s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
  }
}