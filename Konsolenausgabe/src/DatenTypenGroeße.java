/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class DatenTypenGroe�e {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
       byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
       long anzahlSterne = 200_000_000_000L;
    
    // Wie viele Einwohner hat Berlin?
       int bewohnerBerlin = 3_769_000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    // 80 Jaher x 365 Tage = 29.200 Tage, Short potentiell zu klein
       short alterTage = 8_760;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm = 200_000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat? Russland
       int flaecheGroessteLand = 17_098_246;
    
    // Wie gro� ist das kleinste Land der Erde? Malta
    
       short flaecheKleinsteLand = 316;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten + " Planeten");
    
    System.out.println("Anzahl der Sterne in der Milchstra�e: " + anzahlSterne + " Sterne");
    
    System.out.println("Anzahl Bewohner in Berlin: " + bewohnerBerlin + "Bewohner");
    
    System.out.println("Mein Alter: " + alterTage + " Tage");
    
    System.out.println("Das Gewicht des schwersten Tieres der Welt: " + gewichtKilogramm +"kg");
    
    System.out.println("Fl�che des gr��ten Landes der Welt: " + flaecheGroessteLand + "km�");
    
    System.out.println("Fl�che des kleinsten Landes: " + flaecheKleinsteLand + "km�");
    
    System.out.println("\n *******  Ende des Programms  ******* ");
    
  }
}
